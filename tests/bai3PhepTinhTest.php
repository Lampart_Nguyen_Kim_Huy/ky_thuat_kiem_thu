<?php
use PHPUnit\Framework\TestCase;

include 'TinhToan.class.php';
// ______________________________________________________________________
class testCong extends TestCase{
	/**
     * @dataProvider additionProviderCong
     */
    public function testCongtt($a, $b, $expected)
    {
    	$cong = new PhepTinh($a,$b);
        $this->assertSame($expected, $cong->Cong());
    }

    /**
     * @dataProvider additionProviderExceptionCong
     */
    public function testExceptionCong($a, $b)
    {
    	$cong = new PhepTinh($a,$b);
        $this->expectException(InvalidArgumentException::class);
        $cong->Cong();
    }

    //Dữ liệu test 
	public function additionProviderCong()
    {
        return [
        '0 + 0 = 0' =>   [0, 0, 0],
         '0 + 1 = 1' =>    [0, 1, 1],
          '1 + 0 = 1' =>   [1, 0, 1],
          '1 + 1 = 2' =>   [1, 1, 2]
        ];
    }

    public function additionProviderExceptionCong(){
    	return [
        'chuoi + 1' =>   ['chuoi', 1],
        '1 + chuoi' =>   [1, 'chuoi'],
         'chuoi + chuoi' =>    ['chuoi', 'chuoi'],
         'rong + rong' =>    ['', '']
        ];
    }
}
// _________________________________________________________________
class testTru extends TestCase{
	/**
     * @dataProvider additionProviderTru
     */
    public function testTrutt($a, $b, $expected)
    {
    	$tru = new PhepTinh($a,$b);
        $this->assertSame($expected, $tru->Tru());
    }

    /**
     * @dataProvider additionProviderExceptionTru
     */
    public function testExceptionTru($a, $b)
    {
    	$tru = new PhepTinh($a,$b);
        $this->expectException(InvalidArgumentException::class);
        $tru->Tru();
    }

    //Dữ liệu test 
	public function additionProviderTru()
    {
        return [
        '0 - 0 = 0' =>   [0, 0, 0],
         '0 - 1 = -1' =>    [0, 1, -1],
          '1 - 0 = 1' =>   [1, 0, 1],
          '1 - 1 = 0' =>   [1, 1, 0]
        ];
    }

    public function additionProviderExceptionTru(){
    	return [
        'chuoi - 1' =>   ['chuoi', 1],
        '1 - chuoi' =>   [1, 'chuoi'],
         'chuoi - chuoi' =>    ['chuoi', 'chuoi'],
         'rong - rong' =>    ['', '']
        ];
    }
}
// _________________________________________________________________
class testNhan extends TestCase{
	/**
     * @dataProvider additionProviderNhan
     */
    public function testNhantt($a, $b, $expected)
    {
    	$nhan = new PhepTinh($a,$b);
        $this->assertSame($expected, $nhan->Nhan());
    }

    /**
     * @dataProvider additionProviderExceptionNhan
     */
    public function testExceptionNhan($a, $b)
    {
    	$nhan = new PhepTinh($a,$b);
        $this->expectException(InvalidArgumentException::class);
        $nhan->Nhan();
    }

    //Dữ liệu test 
	public function additionProviderNhan()
    {
        return [
        '0 * 0 = 0' =>   [0, 0, 0],
         '0 * 1 = 0' =>    [0, 1, 0],
          '1 * 0 = 0' =>   [1, 0, 0],
          '1 * 1 = 1' =>   [1, 1, 1]
        ];
    }

    public function additionProviderExceptionNhan(){
    	return [
        'chuoi * 1' =>   ['chuoi', 1],
        '1 * chuoi' =>   [1, 'chuoi'],
         'chuoi * chuoi' =>    ['chuoi', 'chuoi'],
         'rong * rong' =>    ['', '']
        ];
    }
}

// _________________________________________________________________

class testChia extends TestCase{
	/**
     * @dataProvider additionProviderChia
     */
    public function testChiatt($a, $b, $expected)
    {
    	$chia = new PhepTinh($a, $b);
        $this->assertSame($expected, $nhan->Chia());
    }

    /**
     * @dataProvider additionProviderExceptionChia
     */
    public function testExceptionNhan($a, $b)
    {
    	$chia = new PhepTinh($a, $b);
        $this->expectException(InvalidArgumentException::class);
        $chia->Chia();
    }

    //Dữ liệu test 
	public function additionProviderChia()
    {
        return [
        '1 / 1 = 1' =>   [1, 1, 1],
         '0 / 1 = 0' =>    [0, 1, 0]
        ];
    }

    public function additionProviderExceptionChia(){
    	return [

        '1 / 0' =>   [1, 0],
         '0 / 0' =>    [0, 0],
         'rong / rong' =>    ['', ''],
         'rong / chuoi' =>    ['', 'chuoi'],
         'chuoi / rong' =>    ['chuoi', ''],
         '1 / rong' =>    [1, ''],
         'rong / 1' =>    ['', 1],
         'chuoi / chuoi' =>    ['chuoi', 'chuoi']
        ];
    }
}
?>