<?php 
class PhepTinh {
	public $a;
	public $b;
	public function __construct($a, $b){
		$this->a = $a;
		$this->b = $b;
	}

	// public function Cong(){
	// 	return  $this->a + $this->b;
	// }
	public function Cong(){
		return  $this->a + $this->b;
	}

	public function Tru(){
		return $this->a - $this->b;
	}

	public function Nhan(){
		return $this->a * $this->b;
	}

	public function Chia(){
		return $this->a / $this->b;
	}
}
?>